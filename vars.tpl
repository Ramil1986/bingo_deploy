---

app_address1: ${ip_addrs1}
app_address2: ${ip_addrs2}
db_address: ${ip_addrsdb}
proxy_address: ${ip_addrproxy}
