# Добавление переменных

locals {
  sa_name      = "ig-sa"
  subnet_name1 = "subnet-1"
  folder_id    = var.folder_id
  vm_user      = "ubuntu"

}

provider "yandex" {
  zone      = "ru-central1-a"
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  token     = var.token
}

resource "yandex_iam_service_account" "ig-sa" {
  name = local.sa_name
}

resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = local.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.ig-sa.id}"
}


resource "yandex_vpc_subnet" "subnet-1" {
  folder_id      = local.folder_id
  name           = local.subnet_name1
  zone           = "ru-central1-a"
  network_id     = var.network
  v4_cidr_blocks = ["192.168.2.0/24"]
}

resource "yandex_vpc_security_group" "alb-sg" {
  name        = "alb-sg"
  network_id  = var.network

  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol       = "TCP"
    description    = "ext-http"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 80
  }

  ingress {
    protocol       = "TCP"
    description    = "ext-https"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 443
  }

  ingress {
    protocol          = "TCP"
    description       = "bingo"
    v4_cidr_blocks    = ["0.0.0.0/0"]
    port              = 19825
  }
}

resource "yandex_vpc_security_group" "alb-vm-sg" {
  name        = "alb-vm-sg"
  network_id  = var.network
  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol          = "TCP"
    description       = "balancer"
    security_group_id = yandex_vpc_security_group.alb-sg.id
    port              = 80
  }
  ingress {
    protocol          = "TCP"
    description       = "balancer"
    security_group_id = yandex_vpc_security_group.alb-sg.id
    port              = 443
  }

  ingress {
    protocol       = "TCP"
    description    = "ssh"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }
  ingress {
    protocol       = "TCP"
    description    = "bingo"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 19825
  }
}

resource "yandex_compute_image" "ubuntu" {
  source_family = "ubuntu-2204-lts"
}

resource "yandex_compute_instance" "app" {
  count              = 2
  service_account_id = yandex_iam_service_account.ig-sa.id

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = "network-ssd"
      size     = 8
    }
  }

  network_interface {
    subnet_id         = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id]
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "db" {
  name = "db"
  service_account_id = yandex_iam_service_account.ig-sa.id
  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = "network-ssd"
      size     = 15
    }
  }

  network_interface {
    subnet_id         = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id]
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "proxy" {
  name = "proxy"
  service_account_id = yandex_iam_service_account.ig-sa.id

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = "network-ssd"
      size     = 8
    }
  }

  network_interface {
    subnet_id         = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id]
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "monitor" {
  name = "monitor"
  service_account_id = yandex_iam_service_account.ig-sa.id

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = "network-ssd"
      size     = 8
    }
  }

  network_interface {
    subnet_id         = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id]
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}


resource "local_file" "inventory" {
  filename = "hosts"
  file_permission = "0644"
  
  content = templatefile("hosts.tpl", {
    ip_addrs = yandex_compute_instance.app[*].network_interface.0.nat_ip_address
    ip_addrsdb = yandex_compute_instance.db.network_interface.0.nat_ip_address
    ip_addrproxy = yandex_compute_instance.proxy.network_interface.0.nat_ip_address
    ip_addrmon = yandex_compute_instance.monitor.network_interface.0.nat_ip_address
  })
}
resource "local_file" "vars" {
  filename = "vars.yml"
  file_permission = "0644"
  
  content = templatefile("vars.tpl", {
    ip_addrs1 = yandex_compute_instance.app[0].network_interface.0.ip_address
    ip_addrs2 = yandex_compute_instance.app[1].network_interface.0.ip_address
    ip_addrsdb = yandex_compute_instance.db.network_interface.0.ip_address
    ip_addrproxy = yandex_compute_instance.proxy.network_interface.0.ip_address
  })
}

output "internal_ip_address_monitor" {
  value = yandex_compute_instance.monitor.network_interface.0.nat_ip_address
}

output "internal_ip_address_proxy" {
  value = yandex_compute_instance.proxy.network_interface.0.nat_ip_address
}




