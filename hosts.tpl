[app]
%{ for addr in ip_addrs ~}
${addr}
%{ endfor ~}
[db]
${ip_addrsdb}
[proxy]
${ip_addrproxy}
[mon]
${ip_addrmon}


